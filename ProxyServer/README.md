# Proxy Server

* [Info Page](http://avimehenwal.in/security/proxy/)
* [squid directives](https://wiki.squid-cache.org/SquidFaq/SquidAcl#ACL_elements)
* Use `tracepath`
* [MTR tool instead of traceroute/tracepath](http://www.bitwizard.nl/mtr/)


```sh
docker-compose config
docker-compose build
docker-compose up
docker-compose exec squid bash
```

Test Proxy

```sh
curl -IL google.com
curl --proxy http://localhost:3128 -IL google.com

tracepath -m 10 -b google.com
mtr --curses google.com
mtr -rw google.com

set --show http_proxy
set --export http_proxy http://localhost:3128
set --erase http_proxy
```