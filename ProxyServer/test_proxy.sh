#!/bin/bash

TARGET=facebook.com

function trace_route {
    mtr -rw $TARGET
}

function curl_req {
    curl --write-out '%{http_code} %{num_connects}' --silent --output /dev/null $TARGET
    echo -e "\twithout proxy $TARGET"
    curl --write-out '%{http_code} %{num_connects}' --silent --proxy http://localhost:3128 --output /dev/null $TARGET
    echo -e "\tWith squid proxy 3128 $TARGET"
    curl --write-out '%{http_code} %{num_connects}' --silent --proxy http://localhost:3129 --output /dev/null $TARGET
    echo -e "\tWith squid proxy 3129 $TARGET"
}

# Test route with proxy and without proxy
curl_req

set --export http_proxy http://localhost:3128
trace_route
set --erase http_proxy
trace_route